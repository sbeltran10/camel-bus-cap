# Capacitación Apache Camel

### Contenido 
[1. Instalación IDE Redhat](#insredhat)  
[2. Conexión a Red Hat Linux](#redhatlinux)  
[3. Instalación subsistema camel](#inscamel)  
[4. Clonar e importar proyecto en el IDE](#clonar)  
[5. Despliegue](#despliegue)  
[6. Ejemplo 1: Manipulación del mensaje y routing](#manipulacion)  
[7. Ejemplo 2: Transformación de datos](#transformacion)  

<a name="insredhat"/>

## 1. Instalación IDE Redhat

Antes de instalar el IDE se debe tener instalada la versión 8 del JDK.

El instalador de Red Hat Developer Studio se puede obtener del siguiente [link](https://github.com/wildfly-extras/wildfly-camel/releases) (Es necesario crear una cuenta gratis Red Hat).
Ejecutar el instalador cuando termine la descarga, seguir los pasos y seleccionar "Red Hat Fuse Tooling" cuando se presente la opción

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/installer1.PNG" width="600px" height="auto" />

<a name="redhatlinux"/>

## 2. Conexión a Red Hat Linux

Los datos de conexión SSH a la máquina de Red Hat que será usada para este ejercicio son los siguientes:

IP: Dada por el instructor

User: bcomwildfly  
Password: bcomwildfly  

<a name="inscamel"/>

## 3. Instalación subsistema camel

Si se desea desplegar el bus de servicios en un nuevo servidor Wildfly 10.1, primero es necesario instalar el subsistema Camel. El subsistema Camel provee al servidor con los componentes necesarios para ejecutar los servicios contenidos en un archivo .war del bus de servicios.

El primer paso es descargar el archivo wildfly-camel-patch 4.9.0 del siguiente [link](https://github.com/wildfly-extras/wildfly-camel/releases) el cual es compatible con la versión 10.1 de Wildfly que es usada en Gerleinco. Si se desea descargar directamente en la máquina de Wildfly, se puede usar el siguiente comando (asegurarse que la herramienta wget este previamente instalada):

```
sudo wget https://github.com/wildfly-extras/wildfly-camel/releases/download/4.9.0/wildfly-camel-patch-4.9.0.tar.gz
```

El segundo paso es descomprimir los contenidos del archivo descargado en la carpeta raíz del servidor Wildfly 10.1:

```
tar -zvxf ~/wildfly-camel-patch-4.9.0.tar.gz  -C /opt/wildfly-10.1.0.Final/
```

Para comprobar que el proceso haya sido exitoso, se debe reiniciar el servidor y luego verificar que en la lista de despliegues haya un paquete llamado hawtio. Este paquete no es necesario para el funcionamiento del bus de servicios entonces es recomendado eliminarlo de los despliegues.

El ultimo paso es correr el siguiente comando desde la carpeta raíz de Wildfly:

```
bin/fuseconfig.sh --configs=camel --enable
```

<a name="clonar"/>

## 4. Clonar e importar proyecto en el IDE

Clonar el proyecto de este repositorio y luego importarlo en el IDE usando File > Import > Maven > Existing Maven Projects. Cuando se importa por primera vez, el proyecto puede tardar un par de minutos en configurarse.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ide1.png" width="600px" height="auto" />

<a name="despliegue"/>

## 5. Despliegue

En esta sección se detallan los pasos a seguir para generar un archivo .war del bus de servicios para luego desplegarlo en el servidor de Wildfly.

### Generación usando la consola de comandos

Para usar este método primero es necesario descargar e instalar [maven](https://maven.apache.org/download.cgi) en el equipo. Luego de asegurarse que maven y el JDK están correctamente instalados, abrir una consola de comandos en la raíz del proyecto y ejecutar el siguiente comando:

```
mvn clean install
```

### Generación usando Red Hat Developer Studio

Hacer click derecho sobre la raíz del proyecto y luego seleccionar Run as > Maven clean, cuando finalice este proceso seleccionar Run as > Maven install.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/deploy1.png" width="600px" height="auto" />

### Despliegue en Wildfly

Cuando se genera un archivo de despliegue exitosamente, el directorio target en la raíz del proyecto es actualizado con la última versión. Para hacer el despliegue en Wildfly solo basta con usar el archivo gersafeii-bus.war en la consola de administración del servidor.

Los credenciales para la consola de administración de Wildfly son los siguientes:

Url: (IP):9000

User: admin  
Password: admin  

Se puede comprobar que él .war se desplego correctamente accediendo a la url:

http://(IP):8080/my-camel-spring-application?name=Camel

Cada vez que se realizan cambios en el bus es necesario generar un nuevo archivo de despliegue.

<a name="manipulacion"/>

## 6. Ejemplo 1: Manipulación del mensaje y routing

En este ejemplo se construirá una ruta simple que guarda un archivo en texto plano en un directorio del sistema a partir de una solicitud HTTP que se le envía al bus.

1. Crear una nueva Ruta usando el menú "Pallete", luego seleccionar "routing" y arrastrar el elemente Route al área de trabajo.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/crear-ruta.png" width="600px" height="auto" />

2. Es recomendado cambiar el nombre por defecto de la ruta, esto se puede hacer en la ventana de propiedades del IDE o directamente en el archivo camel-context.
3. Buscar el componente SEDA en el menú "Pallete" y arrastrarlo dentro de la ruta creada previamente, cambiar el uri del SEDA a "seda:inicio".

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo1-2.PNG" width="200px" height="auto" />

4. Crear una clase llamada EjemploServlet dentro del paquete com.mycompany, se iniciaran procesos en la ruta Ejemplo1 a partir de este servlet. Modificar la clase creada con el siguiente código:
```java
@WebServlet(name = "HttpServiceServletEjemplo", urlPatterns = { "/ejemplo1" }, loadOnStartup = 1)
public class EjemploServlet extends HttpServlet {
  @Resource(lookup = "java:jboss/camel/context/spring-context")
  private CamelContext camelContext;

    @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String modulo = req.getParameter("modulo");
      ServletOutputStream out = res.getOutputStream();
        ProducerTemplate producer = camelContext.createProducerTemplate();
        String result = producer.requestBody("seda:inicio", modulo, String.class);
      out.print(result);
    }
}
```
5. Buscar el componente Bean en el menú "Pallete" y arrastrarlo dentro de la ruta, luego de esto se debe unir el componente SEDA con el componente Bean. Para hacer esto ahí que ubicarse sobre el componente SEDA, seleccionar la flecha y luego arrastrar hasta el componente Bean.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo1-3.PNG" width="250px" height="auto" />

6. Ahora se debe crear una clase Java la cual contendrá la lógica ejecutada por el Bean. Esta clase debe implementar la interfaz Processor para que tenga acceso al objeto Exchange. Modificar la clase creada con el siguiente código:

```java
public class Processor1 implements Processor{

  @Override
  public void process(Exchange exchange) throws Exception {
    String modulo = (String) exchange.getIn().getBody();
    
    if(modulo.equals("maritimas"))
      exchange.getIn().setHeader("evento", "arribo");
    else if(modulo.equals("aduaneras"))
      exchange.getIn().setHeader("evento", "manifestacion");
  }

}
```

7. Para que la lógica pueda ser ejecutada por algún Bean primero es necesario registrar la clase Processor1 en el archivo camel-context. Abrir la fuente del archivo y dentro de la etiqueta beans, antes de la etiqueta camelContext insertar al siguiente línea:

```xml
<bean class="com.mycompany.Processor1" id="processor1"/>
```

8. Asociar la clase Processor1 al Bean creado previamente, para esto se tiene que modificar la propiedad ref del Bean para que sea igual al id declarado en el paso anterior

```xml
<bean id="_bean2" ref="processor1"/>
```

9. En el submenu Routing del menú "Pallete", buscar el elemento Choice y luego arrastrarlo dentro de la ruta, luego buscar el elemento When y arrastrar dos instancias dentro del elemento Choice. Finalmente unir el componente Bean al elemento Choice.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo1-4.PNG" width="500px" height="auto" />

10. Los elementos de routing When usan expresiones booleanas para determinar el camino que va a seguir un exchange. En este ejemplo se evaluará el valor guardando en el header del mensaje en el paso anterior para determinar por donde mandar el exchange.
Modificar la propiedad Expression del primer elemento When, seleccionando javaScript como el lenguaje y luego usar la siguiente expresión:

```javascript
request.headers.get('evento') ===  'arribo'
```

En el segundo elemento When, usar la siguiente expresión:

```javascript
request.headers.get('evento') ===  'manifestacion'
```

11. Agregar un componente Bean dentro de cada elemento When, luego crear las siguientes clases dentro del paquete com.mycompany y asociar cada una a su respectivo Bean.

```java
public class WhenProcessor1 implements Processor{

  @Override
  public void process(Exchange exchange) throws Exception {
    exchange.getIn().setBody(new Date() + " Evento de arribo de op. maritimas");
  }

}
```

```java
public class WhenProcessor2 implements Processor{

  @Override
  public void process(Exchange exchange) throws Exception {
    exchange.getIn().setBody(new Date() + " Evento de manifestacion de op. aduaneras");
  }
}
```

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo1-5.PNG" width="500px" height="auto" />

12. Agregar un componente File dentro de la ruta, conectar el elemento Choice a este componente file. Modificar la propiedad uri del componente con lo siguiente:

```
file:/home/bcomwildfly/bus?autoCreate=true&fileName=evento.txt
```

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo1-6.PNG" width="500px" height="auto" />

<a name="transformacion"/>

## 7. Ejemplo 2: Transformación de datos

En este ejemplo se construirá una ruta que lee archivos .json de una carpeta del sistema, aplica un proceso de conversión a .xml y finalmente los envía a otra carpeta destino.

1. Crear una ruta llamada ejemplo2 y agregar un componente File al inicio de esta. Cambiar la uri del componente por lo siguiente:

```
file:/home/bcomwildfly/fuentesJSON?autoCreate=true
```

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo2-1.PNG" width="300px" height="auto" />

2. Buscar el elemento "Data Transformation" en el submenú "Transformation" del menú "Palette", arrastrarlo dentro de la ruta y cuando se presente el dialogo, usar el ID "ejemplo2Transformation", en "Source Type" seleccionar JSON y en "Target Type" seleccionar XML.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo2-2.PNG" width="600px" height="auto" />

3. En el siguiente dialogo seleccionar "JSON Instance Document" y elegir el archivo ejemplo.json. De este modo el transformador determina el nombre y tipos de datos que existen en el archivo fuente.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo2-3.PNG" width="600px" height="auto" />

4. En el siguiente dialogo seleccionar "JSON Instance Document" y elegir el archivo ejemplo.xml. De este modo el transformador determina el nombre y tipos de datos que existen en el archivo destino.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo2-4.PNG" width="600px" height="auto" />

5. Cuando la transformación se termine de crear, hacer doble click sobre el componente en la ruta del bus. Cuando esto ocurre se muestra una vista en donde presentan los campos disponibles en el archivo fuente y en el archivo destino.
Para este ejemplo solo basta con arrastrar cada campo en el origen con su campo correspondiente en el destino. Al terminar, se debe volver a la vista de camel-context y se debe unir el componente File con el componente de transformación.

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo2-5.PNG" width="600px" height="auto" />

6. Agregar un componente "File" al final de la ruta, modificar la propiedad "Uri" del componente con lo siguiente:

```
file:/home/bcomwildfly/transformacionesXML?autoCreate=true&fileName=manifestacion.xml
```

<img src="https://gitlab.com/sbeltran10/camel-bus-cap/raw/master/readme-resources/ejemplo2-6.PNG" width="300px" height="auto" />